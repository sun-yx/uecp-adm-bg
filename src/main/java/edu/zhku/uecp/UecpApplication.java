package edu.zhku.uecp;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import springfox.documentation.oas.annotations.EnableOpenApi;

import javax.sql.DataSource;

@EnableOpenApi
@SpringBootApplication
public class UecpApplication {
    /*@Bean
    @ConfigurationProperties(prefix = "db")
    //dateSource()作用是注入datasourcebean，同时加载自定义配置项的作用。
    public DataSource dateSource() {
        DruidDataSource dataSource = new DruidDataSource();
        return dataSource;
    }*/
    public static void main(String[] args) {
        SpringApplication.run(UecpApplication.class, args);
    }

}
