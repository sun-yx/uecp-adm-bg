package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface EntAdmRepository extends JpaRepository<EntAdm, EntAdmPrimaryKey> {
    @Query("select ea.EntInfo from EntAdm ea where ea.UserId=?1")
    List<Enterprise> findEntsByAdmId(Integer usrId);
    @Query("select ea.EntInfo from EntAdm ea where ea.UserId=?1")
    Page<Enterprise> findEntsByAdmId(Integer usrId, Pageable pageable);


    @Query("select e from Enterprise e where e.Name like CONCAT('%',:name,'%') and e.Id=any( select ea.EntId from EntAdm ea where ea.UserId=:userid) ")
    Page<Enterprise> findEntsByEntName(@Param("userid") Integer id, @Param("name") String name, Pageable pageable);
    @Query("select e from Enterprise e where e.Name like CONCAT('%',:name,'%')" )
    Page<Enterprise> findEntsByEntName( @Param("name") String name, Pageable pageable);


    @Query("select e from Enterprise e where e.City=:cityId")
    Page<Enterprise> findEntsByCityId(@Param("cityId") Integer cityId, Pageable pageable);
    @Query("select e from Enterprise e where e.City=:cityId and e.Id=any( select ea.EntId from EntAdm ea where ea.UserId=:userid)")
    Page<Enterprise> findEntsByCityId(@Param("userid") Integer id,@Param("cityId") Integer cityId, Pageable pageable);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Enterprise e set e.Name=?2 where e.Id=?1")
    void update(Integer id, String name);
}