package edu.zhku.uecp.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("select u from User u where u.LogName=?1")   //此处表名(User)和列名(LogName)应均为对应@Entity和@Column注解的类和属性名
    User findByLogname(String logname);                 //不用List<User>，LogName是唯一的
    @Query("select u from User u where u.LogName=?1 and u.Role>=128 ")   //此处表名(User)和列名(LogName)应均为对应@Entity和@Column注解的类和属性名
    User findAdmByLogname(String logname);                 //不用List<User>，LogName是唯一的
}
