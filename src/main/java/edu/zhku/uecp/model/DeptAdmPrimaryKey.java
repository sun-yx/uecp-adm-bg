package edu.zhku.uecp.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class DeptAdmPrimaryKey implements Serializable {
    private Integer UserId;
    private Integer DeptId;
}