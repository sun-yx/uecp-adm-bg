package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Blob;

//使用JPA注解配置映射关系
@Data   //由lombok提供，自动生成get、set等方法
@Entity //告诉JPA这是一个实体类（和数据表映射的类）
@Table(name = "user") //@Table来指定和哪个数据表对应;如果省略默认表名就是User；
public class User {

    @Id //这是一个主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)//自增主键，由数据库生成
    @Column(name = "id")                            //省略name默认列名就是属性名
    private Integer Id;

    @NotNull
    @Column(name = "logname",length = 20)   // 登录名，根据JPA命名规范，如name = "LogName"，
                                            // 则数据库操作时列名转换为"log_name"，
                                            // 为避免因名字转换导致运行错误，建议数据表中所有列名均小写
    private String LogName = "usr_";        //建议默认为usr_UserID
    @Column(name = "name",length = 10)      //用户实名
    private String Name;
//Role-由二进制角色位(D1~D8)构成，未认证用户角色为未定Role=0。角色位为：
//    Role=0    未定(默认)
//    Role的D1=1    学生位
//            Role的D2=1    高校教师位
//            Role的D3=1    企业教师位
//    Role的D4~D7    (预留)
//    Role的D8位  系统管理员位
//    默认系统管理员兼具(D8~D1)，可随意切换角色；
//    用户在不同角色间切换后，其主界面(工作台)也应变换。
    @Column(name = "role")
    private Integer Role = 0;
    @NotNull
    @Column(name = "password",length = 10)
    private String Password;
    @Column(name = "mobilephone",length = 11)
    private String MobilePhone;
    @Column(name = "email")
    private String Email;
    @Column(name = "status")
    private Integer Status = 0;     //默认正常
    @Column(name = "verified")
    private Integer Verified = 0;
    @Column(name = "verifier")
    private Integer Verifier = 0;     //默认值应为系统管理员的ID
    @Column(name = "tokensecretkey")    //token加密密钥
    private String TokenSecretKey;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "headimage", columnDefinition="blob", nullable=true)         //用户头像
    private Blob HeadImage;
}
