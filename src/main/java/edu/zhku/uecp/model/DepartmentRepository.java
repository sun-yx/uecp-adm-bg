package edu.zhku.uecp.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DepartmentRepository extends JpaRepository<Department,Integer> {
    @Query("SELECT D from Department D where D.Name=?1")
    Department findByDeptName(String DeptName);
}
