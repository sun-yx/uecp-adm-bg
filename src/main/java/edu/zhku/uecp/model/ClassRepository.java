package edu.zhku.uecp.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClassRepository extends JpaRepository<Class, Integer> {

    @Query("select c from Class c where c.Id=?1")
    Class findClass(Integer classId);
}