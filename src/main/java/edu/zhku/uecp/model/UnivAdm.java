package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "univ_adm")
@IdClass(UnivAdmPrimaryKey.class)   //组合主键
public class UnivAdm {
    @Id
    @Column(name = "userid", nullable = false)
    private Integer UserId;
    @Id
    @Column(name = "univid", nullable = false)
    private Integer UnivId;

    @ManyToOne
    @JoinColumn(name="userid", insertable=false, updatable=false)
    private User UsrInfo;//管理员的基本用户信息，来自用户表*/

    @ManyToOne
    @JoinColumn(name="univid", insertable=false, updatable=false)
    private Universary UnivInfo;//高校信息*/

}

