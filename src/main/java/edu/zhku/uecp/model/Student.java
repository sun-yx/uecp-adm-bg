package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "stu")
public class Student {
    @Id
    @Column(name = "stuid")
    private Integer StudentId;
    @Column(name = "stunum")
    private String StudentNum;
    @Column(name = "classid")
    private Integer ClassId;

    @ManyToOne
    @JoinColumn(name="stuid", insertable=false, updatable=false)
    private User UsrInfo;//基本用户信息，来自用户表*/
    @ManyToOne
    @JoinColumn(name="classid", insertable=false, updatable=false)
    private User ClassInfo;//基本用户信息，来自用户表*/
}
