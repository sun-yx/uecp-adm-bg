package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Entity
@Table(name = "univ")
public class Universary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer Id;

    @Column(name = "name")
    @NotNull
    private String Name;
    @Column(name = "city")
    private Integer City;
    @Column(name = "address")
    private String Address;
    @Column(name = "uscc")
    private String USCC;//统一社会信用代码
    //以下两属性只读，其值由表触发器修改
    @Column(name = "creattime", insertable = false, updatable = false)
    private Date CreateTime;
    @Column(name = "moditime", insertable = false, updatable = false)
    private Date ModiTime;

    @ManyToOne
    @JoinColumn(name="city", insertable=false, updatable=false)
    private Region CityInfo;
}
