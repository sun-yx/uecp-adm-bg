package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnterpriseRepository extends JpaRepository<Enterprise, Integer> {
    @Query("select e from Enterprise e where e.Id=?1")
    Enterprise findEnt(Integer entId);
    @Query("select e from Enterprise e")
    Page<Enterprise> findAll(Pageable pageable);
}