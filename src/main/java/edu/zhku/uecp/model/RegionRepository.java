package edu.zhku.uecp.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegionRepository extends JpaRepository<Region, Integer> {
    @Query("select c from Region c where c.Name=?2 and c.PId in (select p.Id from Region p where p.Name=?1 and p.Level=1)")
    Region findByProvCityName(String provName, String cityName);
    @Query(value = "select p from Region p where p.Level=1 order by p.Pinyin")
    List<Region> findAllProvsOrderByPinyin();
    @Query(value = "select c from Region c where c.PId=?1 order by c.Pinyin")
    List<Region> findAllCitiesInProvOrderByPinyin(Integer PId);
}