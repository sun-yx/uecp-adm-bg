package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UnivAdmRepository extends JpaRepository<UnivAdm, UnivAdmPrimaryKey> {
    @Query("select ua.UnivInfo from UnivAdm ua where ua.UserId=?1")
    List<Universary> findUnivsByAdmId(Integer usrId);
    @Query("select ua.UnivInfo from UnivAdm ua where ua.UserId=?1")
    Page<Universary> findUnivsByAdmId(Integer usrId, Pageable pageable);
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Universary u set u.Name=?2 where u.Id=?1")
    void update(Integer id, String name);
}
