package edu.zhku.uecp.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class EntAdmPrimaryKey implements Serializable {
    private Integer UserId;
    private Integer EntId;
}