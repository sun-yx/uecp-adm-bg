package edu.zhku.uecp.model;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "ent_adm")
@IdClass(EntAdmPrimaryKey.class)   //组合主键
public class EntAdm {
    @Id
    @Column(name = "userid", nullable = false)
    private Integer UserId;
    @Id
    @Column(name = "entid", nullable = false)
    private Integer EntId;

    @ManyToOne
    @JoinColumn(name="userid", insertable=false, updatable=false)
    private User UsrInfo;//管理员的基本用户信息，来自用户表*/

    @ManyToOne
    @JoinColumn(name="entid", insertable=false, updatable=false)
    private Enterprise EntInfo;//高校信息*/
}