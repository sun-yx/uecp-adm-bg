package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "dept_adm")
@IdClass(DeptAdmPrimaryKey.class)   //组合主键
public class DeptAdm {
    @Id
    @Column(name = "userid", nullable = false)
    private Integer UserId;
    @Id
    @Column(name = "deptid", nullable = false)
    private Integer DeptId;

    @ManyToOne
    @JoinColumn(name="userid", insertable=false, updatable=false)
    private User UsrInfo;//管理员的基本用户信息，来自用户表*/

    @ManyToOne
    @JoinColumn(name="deptid", insertable=false, updatable=false)
    private Department DeptInfo;//基本用户信息，来自用户表*/
}