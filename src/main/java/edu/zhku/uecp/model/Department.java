package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name = "dept")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer Id;
    @Column(name = "name")
    private String Name;
    @Column(name = "univid")
    private Integer UnivId;
    //以下两属性只读，其值由表触发器修改
    @Column(name = "creattime", insertable = false, updatable = false)
    private Date CreateTime;
    @Column(name = "moditime", insertable = false, updatable = false)
    private Date ModiTime;

    @ManyToOne
    @JoinColumn(name="univid", insertable=false, updatable=false)
    private Universary UnivInfo;

}