package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UniversaryRepository extends JpaRepository<Universary, Integer> {
    //说明：以下不重写的话，findAll(Pageable pageable)默认方法将会把pageable的
    // Sort排序属性字符串(如"Id")自动转换成小写(如"id")，
    // 与注入代码中的Universary属性名(如"Id")可能不一致，会导致异常。
    @Query("select u from Universary u")
    Page<Universary> findAll(Pageable pageable);
    @Query("select u from Universary u where u.Id=?1")
    Universary findAllUnivs(Integer entId);
    @Query("select u from Universary u where u.Name like CONCAT('%',:name,'%') ")
    Page<Universary> findUnivsByUnivName(@Param("name") String name, Pageable pageable);
    @Query("select u from Universary u where u.Name like CONCAT('%',:name,'%') and u.Id=any( select ua.UnivId from UnivAdm ua where ua.UserId=:userid) ")
    Page<Universary> findUnivsByUnivName(@Param("userid")Integer id,@Param("name") String name, Pageable pageable);
    @Query("select u from Universary u where u.City=?1")
    Page<Universary> findUnivsByCity(Integer cityId, Pageable pageable);
    @Query("select u from Universary u where u.City=?2 and u.Id in (select u.UnivId from UnivAdm u where u.UserId=?1)")
    Page<Universary> findUnivsByUserCity(Integer uid, Integer cityId, Pageable pageable);
    }

