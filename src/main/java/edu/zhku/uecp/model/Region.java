package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "region")
public class Region {
    @javax.persistence.Id
    @Column(name = "id")
    private Integer Id;
    @Column(name = "name")
    private String Name;
    @Column(name = "pid")
    private Integer PId;
    @Column(name = "sname")
    private String SName;
    @Column(name = "level")
    private Integer Level;
    @Column(name = "citycode")
    private String CityCode;
    @Column(name = "yzcode")
    private String YZCode;
    @Column(name = "mername")
    private String MerName;
    @Column(name = "lng")
    private float Longitude;
    @Column(name = "lat")
    private float Latitude;
    @Column(name = "pinyin")
    private String Pinyin;
}