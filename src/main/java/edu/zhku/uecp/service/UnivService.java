package edu.zhku.uecp.service;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import edu.zhku.uecp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UnivService {
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private UnivAdmRepository uaRepo;
    @Autowired
    private EntAdmRepository eaRepo;
    @Autowired
    private DeptAdmRepository daRepo;
    @Autowired
    private UniversaryRepository univRepo;
    @Autowired
    private EnterpriseRepository entRepo;
    @Autowired
    private  RegionRepository regRepo;
    //private static int BATCH_COUNT = 50;
    private int count;

    //保存从Excel文件批量导入的高校数据
    public int saveBatchUnivs(MultipartFile file) throws IOException {
        count = 0;
        EasyExcel.read(file.getInputStream(), UnivExcel.class, new AnalysisEventListener<UnivExcel>() {
            @Override
            public void invoke(UnivExcel univExcel, AnalysisContext analysisContext) {
                Universary univ = new Universary();
                if (univExcel.getProv()!=null && univExcel.getCity()!=null) {
                    Region city = regRepo.findByProvCityName(univExcel.getProv(), univExcel.getCity());
                    if (city != null)
                        univ.setCity(city.getId());
                }
                univ.setName(univExcel.getName());
                univ.setAddress(univExcel.getAddr());
                univ.setUSCC(univExcel.getUscc());
                try {
                    //数据非缓存，逐条插入，速度慢。
                    // 是否要改成数据缓存，每个事务批量插入，保存BATCH_COUNT条记录？若如此，中途插入失败怎么办？
                    univRepo.save(univ);
                    count++;
                }catch (Exception e) {
                    //跳过（数据库已存在同高校名记录）
                }
            }
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                //如批量插入，提交保存最后部分数据
            }
        }).sheet().doRead();
        return count;//返回保存的记录数
    }

    public Map<String, Object> searchUnivByUnivName(User usr, String univName) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        //返回第一页(每页10个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        //如果是系统管理员(usr.getRole() & 0x80)，返回查询到的所有的高校
        if ((usr.getRole() & 0x80) > 0) {
            orders.clear();
            orders.add(new Sort.Order(Sort.Direction.ASC, "Name"));
            sort = Sort.by(orders);
            pageable = PageRequest.of(0, 10, sort);
            orgs.put("univ", univRepo.findUnivsByUnivName(univName, pageable));
        } else { //否则分别查找和添加用户管理高校
            orders.clear();
            orders.add(new Sort.Order(Sort.Direction.ASC, "Name"));
            sort = Sort.by(orders);
            pageable = PageRequest.of(0, 10, sort);
            orgs.put("univ", univRepo.findUnivsByUnivName(usr.getId(), univName, pageable));
        }
        return orgs;
    }
    public Map<String, Object> searchUnivByCityId(User usr, Integer cityId, Integer pageNum) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        //返回第一页(每页10个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "Name"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, 10, sort);

        //如果是系统管理员(usr.getRole() & 0x80)，返回查询到的所有的高校
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("univ", univRepo.findUnivsByCity(cityId, pageable));
        } else { //否则分别查找和添加用户管理高校
           orgs.put("univ", univRepo.findUnivsByUserCity(usr.getId(), cityId, pageable));
       }
        return orgs;
    }

    public void saveOne(Map<String,String> map){
        Universary univ = new Universary();
        if (map.get("city") != null && map.get("prov") != null) {
            Region city = regRepo.findByProvCityName(map.get("city"), map.get("prov"));
            if (city != null)
                univ.setCity(city.getId());
        }
        univ.setName(map.get("name"));
        univ.setAddress(map.get("address"));
        univ.setUSCC(map.get("uscc"));
        univRepo.save(univ);
    }
    public void delUniv(Integer univId) {
        univRepo.deleteById(univId);
    }

    public String upUniv(Integer univId, String monName) {
        String befName;
        befName=univRepo.findAllUnivs(univId).getName();
        uaRepo.update(univId,monName);
        return befName;
    }
    //返回查询到的高校下的所有院系
    public Map<String, Object> searchDeptsByUnivId(Integer univId){
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        //返回第一页(每页10个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "Name"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(0, 10, sort);
        orgs.put("depts", daRepo.findDeptByUnivId(univId, pageable));
        return orgs;
    }


}
