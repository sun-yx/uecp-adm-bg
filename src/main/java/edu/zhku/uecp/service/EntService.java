package edu.zhku.uecp.service;

import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleGotoStatement;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import edu.zhku.uecp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EntService {
    @Autowired
    private RegionRepository regRepo;
    @Autowired
    private EntAdmRepository eaRepo;
    @Autowired
    private EnterpriseRepository entRepo;

    private int count;//读入Excel数据的行数

    //根据企业名模糊查找企业
    public Map<String, Object> searchEntByEntName(User usr, String entName,Integer pageNum) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        //返回第一页(每页10个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "Name"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, 10, sort);
        //如果是系统管理员(usr.getRole() & 0x80)，返回查询到的所有的企业
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("ents", eaRepo.findEntsByEntName(entName, pageable));
        } else { //否则分别查找和添加用户管理企业
            orgs.put("ents", eaRepo.findEntsByEntName(usr.getId(), entName, pageable));
        }
        return orgs;
    }

    //根据地名模糊查找企业
    public Map<String, Object> searchEntByCityId(User usr, Integer cityId, Integer pageNum) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        //返回第一页(每页10个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "Name"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, 10, sort);
        //如果是系统管理员(usr.getRole() & 0x80)
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("ents", eaRepo.findEntsByCityId(cityId, pageable));
        } else { //否则分别查找和添加用户管理企业
            orgs.put("ents", eaRepo.findEntsByCityId(usr.getId(), cityId, pageable));
        }
        return orgs;
    }

    //批量导入企业信息
    public int saveBatchEnts(MultipartFile file) throws IOException {
        count = 0;
        EasyExcel.read(file.getInputStream(), EntExcel.class, new AnalysisEventListener<EntExcel>() {
            @Override
            public void invoke(EntExcel entExcel, AnalysisContext analysisContext) {
                Enterprise ent = new Enterprise();
                if (entExcel.getProv() != null && entExcel.getCity() != null) {
                    Region city = regRepo.findByProvCityName(entExcel.getProv(), entExcel.getCity());
                    if (city != null)
                        ent.setCity(city.getId());
                }
                ent.setName(entExcel.getName());
                ent.setAddress(entExcel.getAddress());
                ent.setUSCC(entExcel.getUscc());
                try {
                    //数据非缓存，逐条插入，速度慢。
                    // 是否要改成数据缓存，每个事务批量插入，保存BATCH_COUNT条记录？若如此，中途插入失败怎么办？
                    entRepo.save(ent);
                    count++;
                } catch (Exception e) {
                    //跳过（数据库已存在同企业名记录）
                }
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                //如批量插入，提交保存最后部分数据
            }
        }).sheet().doRead();
        return count;//返回保存的记录数
    }

    public boolean isExist(Integer entId){
        return entRepo.existsById(entId);
    }

    public void delEnt(Integer entId) {
        entRepo.deleteById(entId);
    }

    public String upEnt(Integer entId, String modName){
        String befName;
        befName=entRepo.findEnt(entId).getName();
        eaRepo.update(entId,modName);
        return befName;
    }

    public void saveOne(Map<String,String> map){
        Enterprise ent = new Enterprise();
        if (map.get("prov") != null && map.get("city") != null) {
            Region city = regRepo.findByProvCityName(map.get("prov"), map.get("city"));
            if (city != null)
                ent.setCity(city.getId());
        }
        ent.setName(map.get("name"));
        ent.setAddress(map.get("address"));
        ent.setUSCC(map.get("uscc"));
        entRepo.save(ent);
    }
}
