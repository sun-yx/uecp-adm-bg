package edu.zhku.uecp;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/*")                  // 设置允许跨域的路径
                .allowedOrigins("http://localhost:8080", "http://localhost:80", "http://114.55.218.93")          // 设置允许跨域请求的域名
                //    .allowedOrigins("http://localhost:8080")          // 设置允许跨域请求的域名
                .allowCredentials(true)                           // 是否允许证书
                .allowedMethods("GET", "POST", "DELETE", "PUT", "HEAD", "OPTIONS")   // 设置允许的方法
                .allowedHeaders("*")                              // 设置允许的header属性
                .exposedHeaders("Origin", "X-Requested-With", "Content-Type", "Accept") //设置允许暴露的Header
                .maxAge(3600);                                   // 跨域允许时间
    }
}