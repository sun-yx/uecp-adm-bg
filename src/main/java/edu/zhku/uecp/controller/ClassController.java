package edu.zhku.uecp.controller;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.AdmService;
import edu.zhku.uecp.service.ClassService;
import edu.zhku.uecp.service.EntService;
import edu.zhku.uecp.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Api(tags = "与院系(机构)管理相关的API接口")  //(swagger)API接口类文档
@Controller
public class ClassController {
    @Autowired
    private AdmService admService;
    @Autowired
    private ClassService classService;
    @Autowired
    private TokenService tokenService;


    @ApiOperation("批量导入班级信息，只有系统管理员和院系管理员可执行该操作!")
    @PostMapping("/batchImportClasses")
    @ResponseBody
    public JSONObject batImpClasses(@RequestParam("file") MultipartFile file, @RequestParam("userId") Integer uid,
                                    @RequestParam("token") String strToken) throws IOException {
        JSONObject json = new JSONObject();
        User usr = admService.getDeptAdminByUsrId(uid);
        if (usr != null &&
                tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0) {
            if (file != null) {
                int count = classService.saveBatchClasses(file);
                json.put("ret", 0);
                json.put("msg", "成功上传文件！新保存" + count + "条记录！");

            } else {
                json.put("ret", 1);
                json.put("msg", "上传文件为空！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;

    }

    @ApiOperation("通过Id删除班级")
    @PostMapping("/deleteClassById")
    @ResponseBody
    public JSONObject delEntById(@RequestParam("userId") Integer uid, @RequestParam("classId") Integer classId,
                                 @RequestParam("token") String strToken){
        JSONObject json=new JSONObject();
        User usr = admService.getDeptAdminByUsrId(uid);
        if (usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0){
            if(classId != null && classService.isExist(classId)) {
                classService.delClass(classId);
                json.put("ret", 0);
                json.put("msg", "成功删除Id为" + classId + "的班级！");
            }else {
                json.put("ret", 1);
                json.put("msg", "找不到该班级！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("通过Id更新班级名")
    @PostMapping("/updateClassById")
    @ResponseBody
    public JSONObject upEntById(@RequestParam("userId") Integer uid, @RequestParam("classId") Integer classId,
                                @RequestParam("aftName") String aftName,@RequestParam("token") String strToken){
        JSONObject json = new JSONObject();
        String befName;
        User usr = admService.getDeptAdminByUsrId(uid);
        if (usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0){
            if(classId != null && classService.isExist(classId)) {
                if(aftName != null) {
                    befName = classService.upEnt(classId, aftName);
                    json.put("ret", 0);
                    json.put("msg", "成功把班级名为 ‘" + befName + "’ 更新为 ‘" + aftName + "’ ！");
                }else {
                    json.put("ret", 3);
                    json.put("msg", "请输入你想修改的名字！");
                    return json;
                }
            }else {
                json.put("ret", 1);
                json.put("msg", "找不到该班级！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("添加单个班级")
    @PostMapping("/addOneClass")
    @ResponseBody
    public JSONObject addOneEnt(@RequestParam("userId") Integer uid,@RequestParam("token") String strToken,
                                @RequestBody Map<String,String> map){
        JSONObject json = new JSONObject();
        User usr = admService.getAdministratorByUsrId(uid);
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0)){
            if(map.get("name") != null && map.get("dept") != null) {
                classService.saveOne(map);
                json.put("ret", 0);
                json.put("msg", "添加成功！");
            }else {
                json.put("ret", 1);
                json.put("msg", "添加失败（缺少班级名或者学院）！");
                return json;
            }
        }else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }
}
