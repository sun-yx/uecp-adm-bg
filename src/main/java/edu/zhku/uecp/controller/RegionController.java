package edu.zhku.uecp.controller;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.RegionRepository;
import edu.zhku.uecp.service.AdmService;
import edu.zhku.uecp.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.Map;

@Api(tags = "与管理员操作相关的测试类")  //(swagger)API接口类文档
@Controller
public class RegionController {
    @Autowired
    private RegionRepository regionRep;

    @ApiOperation("获取所有的省份")
    @PostMapping("/getAllProvs")
    @ResponseBody   //返回数据在ResponseBody中
    public JSONObject getAllProvs() {
        JSONObject json = new JSONObject();
        json.put("ret", 0);
        json.put("msg", "成功获取所有省份信息！");
        json.put("provs", regionRep.findAllProvsOrderByPinyin());
        return json;
    }

    @ApiOperation("获取所有的省份")
    @PostMapping("/getAllCities")
    @ResponseBody   //返回数据在ResponseBody中
    public JSONObject getAllCities(@RequestBody Map<String, String> map) {
        Integer pid = Integer.parseInt(map.get("PId"));
        JSONObject json = new JSONObject();
        json.put("ret", 0);
        json.put("msg", "成功获取城市信息！");
        json.put("cities", regionRep.findAllCitiesInProvOrderByPinyin(pid));
        return json;
    }
}