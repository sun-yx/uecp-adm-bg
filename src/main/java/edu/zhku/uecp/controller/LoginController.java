package edu.zhku.uecp.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ramostear.captcha.HappyCaptcha;
import edu.zhku.uecp.model.DeptAdmRepository;
import edu.zhku.uecp.model.UnivAdmRepository;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.AdmService;
import edu.zhku.uecp.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Blob;

@Api(tags = "登录相关API接口(含获取验证码)")  //(swagger)API接口类文档
@Controller
public class LoginController {
    @Autowired
    private AdmService admService;
    @Autowired
    private TokenService tokenService;

    @ApiOperation("获取验证码")      //(swagger)API接口方法文档
    @GetMapping(value="/getCaptcha")
    public void getCaptcha(HttpServletRequest request, HttpServletResponse response) {
        HappyCaptcha.require(request,response)
                .length(4)
                .width(120)
                .height(60)
                //.type(CaptchaType.NUMBER)//校验码为数字
                .build().finish();
    }

    @ApiOperation("用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name="captcha",value="验证码",required=true,paramType="body"),
            @ApiImplicitParam(name="username",value="用户登录名",required=true,paramType="body"),
            @ApiImplicitParam(name="password",value="用户密码",required=true,paramType="body")
    })
    @PostMapping(value="/login")
    public void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");//已在WebMvcConfig中全局设置

         //验证码校验
        String strParams=new String(request.getInputStream().readAllBytes());
        JSONObject params = JSON.parseObject(strParams);
        String code = params.getString("captcha");
        boolean flag = HappyCaptcha.verification(request, code, true);
        JSONObject json = new JSONObject();
        if (!flag) {
            json.put("err", -1); //此处错误编码和错误消息暂时用硬编码，以后应设置错误常量，所有错误编码和提示全局统一定义
            json.put("msg", "验证码错误");
            response.getWriter().write(json.toString());
            return;
        }
        HappyCaptcha.remove(request);

        //校验用户账号，成功则生成JWT令牌写入json的token字段返回客户端，用户以后访问服务器使用该token
        User usr;
        try {
            usr = admService.getAdministratorByLogname(params.getString("username"));
            if (usr == null) {
                json.put("ret", -2);
                json.put("msg", "用户非管理员");
            } else if(usr.getPassword().equals(params.getString("password"))) {
                if (usr.getStatus() == 0) {
                    json.put("ret", 0);
                    json.put("msg", "登录成功");
                    //服务器端使用带签名的token(JWT)，而非session验证用户身份：
                    json.put("token", tokenService.sign(usr));
                    json.put("realName", usr.getLogName());
                    json.put("userId", usr.getId());
                    Blob headImg = usr.getHeadImage();
                    if (headImg != null)//将头像数据用Base64编码
                        json.put("headImg", new String(
                                Base64.encodeBase64(headImg.getBytes(1, (int)headImg.length()))));
                }
                else {
                    json.put("ret", -3);
                    json.put("msg", "用户状态异常，请联系管理员");
                }
            } else {
                json.put("ret", -4);
                json.put("msg", "密码错误");
            }
        }
        catch (Exception e){
            e.printStackTrace();
            json.put("ret", -5);
            json.put("msg", "数据访问异常");
        }
        response.getWriter().write(json.toString());
    }

}
