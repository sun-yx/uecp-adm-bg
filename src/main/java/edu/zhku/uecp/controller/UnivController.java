package edu.zhku.uecp.controller;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.AdmService;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.UnivService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Api(tags = "与高校(机构)管理相关的API接口")  //(swagger)API接口类文档
@Controller
public class UnivController {
    @Autowired
    private AdmService admService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private UnivService univService;

    @ApiOperation("批量导入高校信息，只有系统管理员可执行该操作!")
    @PostMapping(value="/batchImportUnivs")
    @ResponseBody
    public JSONObject batImpUnivs(@RequestParam("file") MultipartFile file, @RequestParam("userId") Integer uid,
                                  @RequestParam("token") String strToken) throws IOException {
        JSONObject json = new JSONObject();
        User usr = admService.getAdministratorByUsrId(uid);
        if ((usr != null && (usr.getRole() & 0x80)>0) &&
                tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0) {
            if (file != null) {
                int count = univService.saveBatchUnivs(file);
                json.put("ret", 0);
                json.put("msg", "成功上传文件！新保存" +  count + "条记录！");

            } else {
                json.put("ret", 1);
                json.put("msg", "上传文件为空！");
                return json;
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }
    @ApiOperation("只是一个测试，在用户登录后验证JWT token签名，判断是否管理员信息，如是则返回其管理机构信息")
    @PostMapping("/searchUnivByUnivName")
    @ResponseBody   //返回数据在ResponseBody中
    public JSONObject searchUnivByUnivName(@RequestBody Map<String, String> map) {
        //获取管理员信息
        JSONObject json = new JSONObject();//用于输出res data
        Integer usrId;
        String strToken;
        String univName;
        if (map.containsKey("userId") && map.containsKey("token") && map.containsKey("univName")) {
            usrId = Integer.parseInt(map.get("userId"));
            strToken = map.get("token");
            univName = map.get("univName");
            User usr = admService.getUnivAdminByUsrId(usrId);
            if (usr == null) {
                json.put("ret", 1);
                json.put("msg", "非管理员或者高校管理员，禁止使用后台管理系统！");
                return json;
            }
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 2);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            Map<String, Object> orgs = univService.searchUnivByUnivName(usr,univName);
            json.put("ret", 0);
            json.put("msg", "成功获取管理员信息！");
            json.put("orgs", orgs);
            return json;
        }
        json.put("ret", 3);
        json.put("msg", "缺少用户ID或token或者univName！");
        return json;
    }
        @ApiOperation("通过地名模糊查找企业")
        @PostMapping("/searchUnivByCityId")
        @ResponseBody   //返回数据在ResponseBody中
        public JSONObject searchUnivByCityName(@RequestBody Map<String, String> map) throws IOException{

            JSONObject json = new JSONObject();//用于输出res data
            Integer usrId;
            String strToken;
            Integer cityId, pageNum;
            if (map.containsKey("userId") && map.containsKey("token") &&
                    (map.containsKey("cityId") && (map.containsKey("pageNum")))) {
                usrId = Integer.parseInt(map.get("userId"));
                strToken = map.get("token");
                cityId = Integer.parseInt(map.get("cityId"));
                pageNum = Integer.parseInt(map.get("pageNum"));
                User usr = admService.getUnivAdminByUsrId(usrId);
                if (usr == null) {
                    json.put("ret", 1);
                    json.put("msg", "非系统管理员或者高校管理员，禁止使用后台管理系统！");
                    return json;
                }
                if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                    json.put("ret", 2);
                    json.put("msg", "token签名校验失败！");
                    return json;
                }
                Map<String, Object> orgs = univService.searchUnivByCityId(usr, cityId, pageNum);
                json.put("ret", 0);
                json.put("msg", "成功获取管理员信息！");
                json.put("orgs", orgs);
                return json;
            }
            json.put("ret", 3);
            json.put("msg", "缺少用户ID或token或(cityId或pageNUm)！");
            return json;
        }
    @ApiOperation("添加单个高校")
    @PostMapping("/addOneUniv")
    @ResponseBody
    public JSONObject addOneUniv(@RequestParam("userId") Integer uid,@RequestParam("token") String strToken,
                                @RequestBody Map<String,String> map){
        JSONObject json = new JSONObject();
        User usr = admService.getAdministratorByUsrId(uid);
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0)){
            if(map.get("name") != null) { //只有名字不能为空
                univService.saveOne(map);
                json.put("ret", 0);
                json.put("msg", "添加成功！");
            }else {
                json.put("ret", 1);
                json.put("msg", "添加失败（没传入添加的高校）！");
                return json;
            }
        }else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }
    @ApiOperation("通过Id删除高校")
    @PostMapping("/deleteUnivById")
    @ResponseBody
    public JSONObject delUnivById(@RequestParam("userId") Integer uid, @RequestParam("univId") Integer univId,
                                 @RequestParam("token") String strToken){
        JSONObject json=new JSONObject();
        User usr = admService.getUnivAdminByUsrId(uid);
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0)){
            if(univId != null) {
                univService.delUniv(univId);
                json.put("ret", 0);
                json.put("msg", "成功删除Id为" + univId + "的高校！");
            }else {
                json.put("ret", 1);
                json.put("msg", "找不到该高校！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("通过Id更新高校名")
    @PostMapping("/updateUnivById")
    @ResponseBody
    public JSONObject upUnivById(@RequestParam("userId") Integer uid, @RequestParam("univId") Integer univId,
                                @RequestParam("newName") String newName,@RequestParam("token") String strToken){
        JSONObject json = new JSONObject();
        String befName;
        User usr = admService.getUnivAdminByUsrId(uid);
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0)){
            if(univId != null) {
                if(newName != null) {
                    befName = univService.upUniv(univId, newName);
                    json.put("ret", 0);
                    json.put("msg", "成功把高校名为 ‘" + befName + "’ 更新为 ‘" + newName + "’ ！");
                }else {
                    json.put("ret", 3);
                    json.put("msg", "请输入你想修改的名字！");
                    return json;
                }
            }else {
                json.put("ret", 1);
                json.put("msg", "找不到该高校！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("按高校ID查询其所有院系")
    @PostMapping(value="/searchDeptsByUnivId")
    @ResponseBody
    public JSONObject searchDeptsByUnivId(@RequestParam("userId") Integer uid,
                                          @RequestParam("token") String strToken,
                                          @RequestParam("univId")Integer univId){
        JSONObject json = new JSONObject();//用于输出res data
        User usr=admService.getUnivAdminByUsrId(uid);
        if(usr != null
                && tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) == 0){
            if(univId != null){
                Map<String, Object> orgs =univService.searchDeptsByUnivId(univId);
                json.put("ret", 0);
                json.put("msg", "成功找到高校ID为"+ univId +"的所有院系！");
                json.put("orgs", orgs);
            }else {
                json.put("ret", 1);
                json.put("msg", "缺少高校ID！");
            }
        }else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }
    }
