package edu.zhku.uecp.controller;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.AdmService;
import edu.zhku.uecp.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.Map;

@Api(tags = "与管理员操作相关的测试类")  //(swagger)API接口类文档
@Controller
public class AdmController {
    @Autowired
    private AdmService admService;
    @Autowired
    private TokenService tokenService;

    @ApiOperation("只是一个测试，在用户登录后验证JWT token签名，判断是否管理员信息，如是则返回其管理机构信息")
    @PostMapping("/admInfo")
    @ResponseBody   //返回数据在ResponseBody中
    public JSONObject admInfo(@RequestBody Map<String, String> map) throws IOException {
        //获取管理员信息
        JSONObject json = new JSONObject();//用于输出res data
        Integer usrId;
        String strToken;
        if (map.containsKey("userId") && map.containsKey("token")) {
            //Token tokenVerifer = new TokenService();//new...()会导致Token中的UserRepository无法注入
            usrId = Integer.parseInt(map.get("userId"));
            strToken = map.get("token");
            User usr = admService.getAdministratorByUsrId(usrId);
            if (usr == null) {
                json.put("ret", 1);
                json.put("msg", "非管理员，禁止使用后台管理系统！");
                return json;
            }
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 2);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            Map<String, Object> orgs = admService.getOrgsByAdmId(usr);
            json.put("ret", 0);
            json.put("msg", "成功获取管理员信息！");
            json.put("orgs", orgs);
            return json;
        }
        json.put("ret", 3);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    //修改日期：2021-4-21
    @ApiOperation("获取管理员管理的三类机构(高校、企业和院系)信息，结果分页(每页10条记录)，返回第一页的内容")
    @PostMapping("/getAdmOrgs")
    @ResponseBody   //返回数据在ResponseBody中
    public JSONObject getAdmOrgs(@RequestBody Map<String, String> map) throws IOException {
        //获取管理员信息
        JSONObject json = new JSONObject();//用于输出res data
        Integer usrId;
        String strToken;
        Integer pageNum = 0;
        if (map.containsKey("userId") && map.containsKey("token")) {
            usrId = Integer.parseInt(map.get("userId"));
            pageNum = Integer.parseInt(map.get("pageNum"));
            strToken = map.get("token");
            User usr = admService.getAdministratorByUsrId(usrId);
            if (usr == null) {
                json.put("ret", 1);
                json.put("msg", "非管理员，禁止使用后台管理系统！");
                return json;
            }
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 2);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            Map<String, Object> orgs = admService.getOrgsOrderById(usr, pageNum);
            json.put("ret", 0);
            json.put("msg", "成功获取管理员信息！");
            json.put("orgs", orgs);
            return json;
        }
        json.put("ret", 3);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }
}

