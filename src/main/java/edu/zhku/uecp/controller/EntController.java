package edu.zhku.uecp.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.ClassRepository;
import edu.zhku.uecp.model.EntAdmRepository;
import edu.zhku.uecp.model.Enterprise;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.AdmService;
import edu.zhku.uecp.service.EntService;
import edu.zhku.uecp.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Api(tags = "与企业(机构)管理相关的API接口")  //(swagger)API接口类文档
@Controller
public class EntController {
    @Autowired
    private AdmService admService;
    @Autowired
    private EntService entService;
    @Autowired
    private TokenService tokenService;

    @ApiOperation("通过企业名模糊查找企业")
    @PostMapping("/searchEntByEntName")
    @ResponseBody   //返回数据在ResponseBody中
    public JSONObject searchEntByEntName(@RequestBody Map<String, String> map) {
        JSONObject json = new JSONObject();//用于输出res data
        Integer usrId;
        String strToken;
        String entName;
        Integer pageNum;
        if (map.containsKey("userId") && map.containsKey("token") && map.containsKey("entName")) {
            //Token tokenVerifer = new TokenService();//new...()会导致Token中的UserRepository无法注入
            usrId = Integer.parseInt(map.get("userId"));
            pageNum = Integer.parseInt(map.get("pageNum"));
            strToken = map.get("token");
            entName = map.get("entName");
            User usr = admService.getEntAdminByUsrId(usrId);
            if (usr == null) {
                json.put("ret", 1);
                json.put("msg", "非系统管理员或者企业管理员，禁止操作！");
                return json;
            }
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 2);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            Map<String, Object> orgs = entService.searchEntByEntName(usr,entName,pageNum);
            json.put("ret", 0);
            json.put("msg", "成功获取得到企业信息！");
            json.put("orgs", orgs);
            return json;
        }
        json.put("ret", 3);
        json.put("msg", "缺少用户ID或token或entName！");
        return json;
    }

    @ApiOperation("通过地名模糊查找企业")
    @PostMapping("/searchEntByCityId")
    @ResponseBody   //返回数据在ResponseBody中
    public JSONObject searchEntByCityId(@RequestBody Map<String, String> map) throws IOException{

        JSONObject json = new JSONObject();//用于输出res data
        Integer usrId;
        Integer pageNum;
        String strToken;
        Integer cityId;
        if (map.containsKey("userId") && map.containsKey("token") &&
                map.containsKey("cityId")) {
            usrId = Integer.parseInt(map.get("userId"));
            pageNum = Integer.parseInt(map.get("pageNum"));
            strToken = map.get("token");
            cityId = Integer.parseInt(map.get("cityId"));;
            User usr = admService.getEntAdminByUsrId(usrId);
            if (usr == null) {
                json.put("ret", 1);
                json.put("msg", "非系统管理员或者企业管理员，禁止使用后台管理系统！");
                return json;
            }
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 2);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            Map<String, Object> orgs = entService.searchEntByCityId(usr, cityId,pageNum);
            json.put("ret", 0);
            json.put("msg", "成功得到企业信息！");
            json.put("orgs", orgs);
            return json;
        }
        json.put("ret", 3);
        json.put("msg", "缺少用户ID或token或cityId！");
        return json;
    }

    @ApiOperation("批量导入企业信息，只有系统管理员可执行该操作!")
    @PostMapping("/batchImportEnts")
    @ResponseBody
    public JSONObject batImpEnts(@RequestParam("file") MultipartFile file, @RequestParam("userId") Integer uid,
                                 @RequestParam("token") String strToken) throws IOException {
        JSONObject json = new JSONObject();
        User usr = admService.getAdministratorByUsrId(uid);
        if ((usr != null && (usr.getRole() & 0x80)>0) &&
                tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0) {
                if (file != null) {
                    int count = entService.saveBatchEnts(file);
                    json.put("ret", 0);
                    json.put("msg", "成功上传文件！新保存" + count + "条记录！");

                } else {
                    json.put("ret", 1);
                    json.put("msg", "上传文件为空！");
                    return json;
                }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;

    }

    @ApiOperation("通过Id删除企业")
    @PostMapping("/deleteEntById")
    @ResponseBody
    public JSONObject delEntById(@RequestParam("userId") Integer uid, @RequestParam("entId") Integer entId,
                                 @RequestParam("token") String strToken){
        JSONObject json=new JSONObject();
        User usr = admService.getEntAdminByUsrId(uid);
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0)){
            if(entId != null && entService.isExist(entId)) {
                entService.delEnt(entId);
                json.put("ret", 0);
                json.put("msg", "成功删除Id为" + entId + "的企业！");
            }else {
                json.put("ret", 1);
                json.put("msg", "找不到该企业！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("通过Id更新企业名")
    @PostMapping("/updateEntById")
    @ResponseBody
    public JSONObject upEntById(@RequestParam("userId") Integer uid, @RequestParam("entId") Integer entId,
                                @RequestParam("aftName") String aftName,@RequestParam("token") String strToken){
        JSONObject json = new JSONObject();
        String befName;
        User usr = admService.getEntAdminByUsrId(uid);
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0)){
            if(entId != null && entService.isExist(entId)) {
                if(aftName != null) {
                    befName = entService.upEnt(entId, aftName);
                    json.put("ret", 0);
                    json.put("msg", "成功把企业名为 ‘" + befName + "’ 更新为 ‘" + aftName + "’ ！");
                }else {
                    json.put("ret", 3);
                    json.put("msg", "没有修改的企业名字！");
                    return json;
                }
            }else {
                json.put("ret", 1);
                json.put("msg", "找不到该企业！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("添加单个企业")
    @PostMapping("/addOneEnt")
    @ResponseBody
    public JSONObject addOneEnt(@RequestParam("userId") Integer uid,@RequestParam("token") String strToken,
                                @RequestBody Map<String,String> map){
        JSONObject json = new JSONObject();
        User usr = admService.getAdministratorByUsrId(uid);
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0)){
            if(map.get("name") != null) { //只有名字不能为空
                entService.saveOne(map);
                json.put("ret", 0);
                json.put("msg", "添加成功！");
            }else {
                json.put("ret", 1);
                json.put("msg", "添加失败（没传入添加的企业）！");
                return json;
            }
        }else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }
}
